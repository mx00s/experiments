mod yak_shave;  // example library with tracing instrumentation
mod readme_examples;
mod async_tracing;

// TODO: experiment with the following crates
//  - [x] tracing-futures
//  - [ ] tracing-timing
//  - [ ] tracing-log
//  - [ ] tracing-opentelemetry
//  - [ ] tracing-coz
//  - [ ] tracing-honeycomb

fn main() {
    readme_examples::main();
}
