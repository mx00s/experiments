use tracing::{instrument, info, info_span, Level};
use tracing_futures::{Instrument, WithSubscriber};
use tracing_subscriber::FmtSubscriber;

use async_std::task;

pub(crate) fn demo() {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    tracing::subscriber::with_default(subscriber, || {
        let span = info_span!("async_demo");
        let _enter = span.enter();

        let handle1 = task::spawn(internal_async_tracing().with_current_subscriber());
        let handle2 = task::spawn(function_async_tracing().with_current_subscriber());
        let handle3 = async {
            info!("Joining tasks and summing results");
            handle1.await + handle2.await
        };

        let sum = task::block_on(handle3.in_current_span());

        info!(result_sum = sum, "Finished both async tasks");
    });
}

async fn internal_async_tracing() -> i32 {
    let my_future = async {
        info!("Executing async block");
        5
    };

    my_future
        .instrument(tracing::info_span!("my_future"))
        .await
}

#[instrument]
async fn function_async_tracing() -> i32 {
    info!("Executing async function");
    5
}
