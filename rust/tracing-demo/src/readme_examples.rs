use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;
use crate::{yak_shave, async_tracing};

pub(crate) fn main() {
    println!("Starting basic demo");
    basic();

    println!("\nStarting scoped subscriber demo");
    scoped_subscriber();

    println!("\nStarting async demo");
    async_tracing::demo();
}

fn basic() {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    // The original README example uses `set_global_default`, but it changes the behavior of other
    // examples. To avoid that we'll use `set_default`.
    //
    // Library authors: take note. The docs for `set_global_default` reiterate this point.
    //
    // > Libraries should NOT call set_global_default()! That will cause conflicts when executables
    // > try to set them later.

    /*
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");
    */

    // `subscriber` is default for the lifetime of `_subscriber`.
    let _subscriber = tracing::subscriber::set_default(subscriber);

    let number_of_yaks = 3;
    // tracing event outside any spans:
    info!(number_of_yaks, "preparing to shave yaks.");

    let number_shaved = yak_shave::shave_all(number_of_yaks);
    info!(
        all_yaks_shaved = number_shaved == number_of_yaks,
        "yak shaving completed."
    );
}

fn scoped_subscriber() {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    tracing::subscriber::with_default(subscriber, || {
        info!("This will be logged to stdout");
    });

    info!("This will _not_ be logged to stdout");
}

