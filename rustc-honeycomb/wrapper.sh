#!/usr/bin/env bash

set -e

RUSTC_BIN="$1"; shift
RUSTC_ARGS="$@"

HONEYCOMB_API_KEY="<redacted>"
HONEYCOMB_ENDPOINT="https://api.honeycomb.io/1/events/rustc%20wrapper"

SERVICE_NAME="$($RUSTC_BIN --version)"

################################################################################

SPAN_START_NS="$(date +%s%N)"

"${RUSTC_BIN}" $RUSTC_ARGS
STATUS_CODE="$?"

SPAN_DURATION_MS=$(echo "($(date +%s%N) - $SPAN_START_NS) / 1000000" | bc)

################################################################################

# naive quote-escaping
RUSTC_ESCAPED_ARGS="$(echo $RUSTC_ARGS | sed 's/"/\\"/g')"
CRATE_NAME="$(echo $RUSTC_ESCAPED_ARGS | grep -ow "\-\-crate\-name [^ $]*" | col2)"

JSON_PAYLOAD=$(cat <<EOF
{
  "name": "$CRATE_NAME",
  "trace.span_id": "$$",
  "trace.trace_id": "$PPID",
  "trace.parent_id": "$PPID",
  "service_name": "$SERVICE_NAME",
  "duration_ms": "$SPAN_DURATION_MS",
  "rustc.bin": "$RUSTC_BIN",
  "rustc.args": "$RUSTC_ESCAPED_ARGS"
}
EOF
	    )

echo "$JSON_PAYLOAD" >> /tmp/rustc-honeycomb

echo "$JSON_PAYLOAD" | curl -s -i \
     -X POST "${HONEYCOMB_ENDPOINT}" \
     -H "X-Honeycomb-Team: ${HONEYCOMB_API_KEY}" \
     -d @- 2>&1 >> /tmp/rustc-honeycomb

exit $STATUS_CODE
