rustc-honeycomb
===============

_a `rustc` wrapper which captures trace data and sends it to [Honeycomb](https://www.honeycomb.io)_

[Rust](https://www.rust-lang.org/) can be slow to compile large projects, and it's not always clear where most time and resources are spent. Variation in hardware, build profiles, toolchains, enabled features, and caching optimizations all impact compilation performance. If the compiler were instrumented to report this metadata and how long its high-level operations take when building a large project we could more readily investigate sources of overhead.

The `cargo` build tool commonly used for Rust projects supports a [`RUSTC_WRAPPER` environment variable](https://doc.rust-lang.org/cargo/reference/environment-variables.html), so all of its calls to the `rustc` compiler can be intercepted. Although this mechanism doesn't reveal any information about what `rustc` does internally each time it's called, it enables us to see when `cargo` invokes `rustc`, what arguments it passes, and how long it takes to run. This may be a useful starting point.

[Honeycomb](https://www.honeycomb.io/) is a useful service for trace observability. Metadata about events, such as `rustc` executions, can be captured and forward to a configured HTTP endpoint. Then Honeycomb's web application provides interactive tools to discover useful information.


Usage
-----

Update the `HONEYCOMB_API_KEY` and `HONEYCOMB_ENDPOINT` variables in the script. Before running commands like `cargo build` ensure the `RUSTC_WRAPPER` environment variable is set to the path of the wrapper script. Consider removing the wrapper log file beforehand as well.

```
$ export RUSTC_WRAPPER="/usr/local/src/experiments/rustc-honeycomb/wrapper.sh"
$ rm -f /tmp/rustc-honeycomb
$ cargo build
```

Take a moment to verify no errors are reported in the wrapper log, e.g. HTTP 400s, concerning JSON responses, or timeouts.

```
$ less /tmp/rustc-honeycomb
```

Lastly, load the Honeycomb dataset associated with the configured `HONEYCOMB_ENDPOINT` and explore.

Here's an example `cargo build` trace from one of my Rust projects.

![example trace](./example-trace.png)