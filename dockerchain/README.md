dockerchain
===========

Hypothesis: *If every docker image (except the first) is built from its predecessor in a chain of successive images with writes to the same file each time, then the size of the docker images will continually grow.*


Running an experiment
---------------------

To try to build a chain of 130 docker images, each writing 4096 bytes to a file:

    ./run.sh dockerchain 130 4096

In practice this fails whil building dockerchain:123 because Docker. A helpful [comment](https://github.com/docker/docker.github.io/issues/8230#issuecomment-468630187) on GitHub explains why. Remember, some of the layers are coming from the `alpine:latest` image used in the genesis image.


Size Analysis
-------------

Assuming you ran the example command until it failed, you should have `dockerchain:<ROUND>` for all values of ROUND between 0 and 122, inclusive. To find the size difference between the earliest (genesis) image and the latest (ROUND=122) I ran the following:

    $ GENESIS_SIZE=$(docker inspect dockerchain:0 | jq '.[].Size')
    $ ROUND_122_SIZE=$(docker inspect dockerchain:122 | jq '.[].Size')
    $ echo "($ROUND_122_SIZE - $GENESIS_SIZE) / 1024 / 1024" | bc -l
    29.78515625000000000000

So, starting from the alpine image used to create genesis and ending with round 122 the image size increased by ~30M. Per image, on average that's an increase of ...

    $ echo "($ROUND_122_SIZE - $GENESIS_SIZE) / 133 / 1024" | bc -l
    229.32330827067669172932

about 229K. Each round created an image where the only change was writing 4096 bytes to the image filesystem. In this case there's about 225K of overhead per image. With more writes we should expect a higher rate of image size increase.
