#!/bin/sh

set -ex

ROUND=$1   # image build iteration
SIZE=$2    # size of dynamic artifact file in bytes

CACHE_DIR="$HOME/.cache"
TARGET="${CACHE_DIR}/dynamic-artifacts"

# Each round alternate what byte is written to the target
COIN=$(( $ROUND % 2 ))
if [ $COIN -eq 0 ]; then
    BYTE="\x00"
else
    BYTE="\xFF"
fi

echo "${ROUND}: Writing ${SIZE} ${BYTE}s to ${TARGET} ..."
mkdir -p "${CACHE_DIR}"
for i in $(seq 1 $SIZE); do
    printf "$BYTE" >> "$TARGET"
done
