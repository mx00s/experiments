#!/bin/sh
set -ex

IMAGE_NAME=$1
ROUNDS=$2
TARGET_SIZE=$3

echo "Building genesis image: ${IMAGE_NAME}:0 ..."
docker build \
    -t "${IMAGE_NAME}:0" \
    --build-arg TARGET_SIZE="${TARGET_SIZE}" \
    --build-arg ROUND=0 \
    ./genesis-image

echo "Building chain of ${ROUNDS} images, each built from the previous ..."
for round in $(seq 1 $ROUNDS); do
    echo "  Round: ${round} ..."
    docker build \
        -t "${IMAGE_NAME}:${round}" \
        --build-arg IMAGE_NAME="${IMAGE_NAME}" \
        --build-arg ROUND="${round}" \
        --build-arg PREV_IMAGE_TAG="$(($round - 1))" \
        --build-arg TARGET_SIZE="${TARGET_SIZE}" \
        ./dockerchain-image
done
